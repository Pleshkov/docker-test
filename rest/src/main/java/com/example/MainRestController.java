package com.example;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by pleshkov on 30.04.2020
 */
@RestController
public class MainRestController {

    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/")
    public String index() {
        return "RestApi is working...";
    }

    @GetMapping("/request")
    public Long getCounter(){
        return counter.incrementAndGet();
    }
}
