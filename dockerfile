FROM goodvvin/tomcat:v1
LABEL maintainer="goodvviin@mail.ru"
RUN rm -rf /usr/local/tomcat/webapps/ROOT
COPY out/ROOT.war /usr/local/tomcat/webapps/ROOT.war
COPY out/api.war /usr/local/tomcat/webapps/api.war
EXPOSE 8080
CMD ["catalina.sh", "run"]